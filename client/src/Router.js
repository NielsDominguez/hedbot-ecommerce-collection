import React from "react"
import { Switch, Route } from "react-router-dom"
import Empty from "./components/Empty"
import Loadable from "react-loadable"

const Loading = () => <div style={{ height: "1000px" }} />

const HomeContainers = Loadable({
  loader: () => import("./containers/HomeContainers"),
  loading: Loading
})

const Router = () => (
  <div>
    <Switch>
      <Route exact path="/" component={HomeContainers} />
      <Route exact path="/:token" component={HomeContainers} />
    </Switch>
  </div>
)

export default Router
