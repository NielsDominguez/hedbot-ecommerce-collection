import React from "react"
import ExpansionPanel from "@material-ui/core/ExpansionPanel"
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary"
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails"
import Typography from "@material-ui/core/Typography"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import Button from "@material-ui/core/Button"

const CatalogsList = ({
  state,
  dispatch,
  _connectPage,
  pageInfo,
  setShowSuccess
}) => {
  return (
    <div>
      {state.catalogsList !== undefined &&
        state.catalogsList.map((x, i) => {
          return (
            <ExpansionPanel key={i}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography>
                  Catalog #{i + 1} - {x.name} ({x.product_count} products)
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography style={{ width: "50%" }}>
                  <b>Products list:</b>
                </Typography>

                <Typography>
                  {x.products !== undefined &&
                    x.products.data !== undefined &&
                    x.products.data.map((product, index) => (
                      <div key={index}>
                        <b>#{index}// </b>
                        id:{product.id} || name:{product.name} || availability:
                        {product.availability} || category:{" "}
                        {product.category === undefined ? (
                          <b style={{ color: "red" }}>
                            {" "}
                            ! NO CATEGORY SELECTED !
                          </b>
                        ) : (
                          product.category
                        )}
                      </div>
                    ))}
                  <Button
                    fullWidth
                    variant="contained"
                    style={{ marginTop: 15 }}
                    color="primary"
                    disabled={
                      x.products === undefined ||
                      (x.products !== undefined &&
                        x.products.data === undefined) ||
                      (x.products !== undefined &&
                        x.products.data !== undefined &&
                        x.products.data.filter(z => z.category) ===
                          undefined) ||
                      (x.products !== undefined &&
                        x.products.data !== undefined &&
                        x.products.data.filter(z => z.category) !== undefined &&
                        x.products.data.filter(z => z.category === undefined)
                          .length > 0)
                    }
                    onClick={() => {
                      _connectPage(
                        pageInfo.id,
                        pageInfo.access_token,
                        pageInfo.name,
                        state.businessId,
                        x.id
                      )
                      setShowSuccess(true)
                    }}
                  >
                    select this catalog
                  </Button>
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          )
        })}
    </div>
  )
}

export default CatalogsList
