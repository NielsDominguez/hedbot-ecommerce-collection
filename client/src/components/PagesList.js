import React from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction"
import ListItemText from "@material-ui/core/ListItemText"
import Avatar from "@material-ui/core/Avatar"
import IconButton from "@material-ui/core/IconButton"
import Usb from "@material-ui/icons/Usb"
import Grid from "@material-ui/core/Grid"

const PagesList = ({ pagesList, _connectPage, setPageInfo }) => {
  return (
    <List>
      {pagesList.map(
        ({ name, access_token, picture, id, is_webhooks_subscribed }) => (
          <ListItem
            key={id}
            className="itemList"
            onClick={() =>
              setPageInfo({
                name,
                access_token,
                picture,
                id,
                is_webhooks_subscribed
              })
            }
          >
            <ListItemAvatar>
              <Avatar alt="Remy Sharp" src={picture.data.url} />
            </ListItemAvatar>
            <ListItemText
              primary={name}
              secondary={is_webhooks_subscribed && "connected to webhook"}
            />
          </ListItem>
        )
      )}
    </List>
  )
}

export default PagesList
