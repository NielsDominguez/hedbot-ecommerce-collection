import React, { useState, useEffect } from "react"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import CatalogsList from "./CatalogsList"
import SuccessConnected from "./SuccessConnected"
import axios from "axios"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"

const SelectedPage = ({
  pageInfo,
  _catalogsList,
  catalogsList,
  state,
  dispatch,
  _connectPage,
  _pauseResume
}) => {
  const [isActive, setIsActive] = useState(false)
  const [showSuccess, setShowSuccess] = useState(false)
  useEffect(() => {
    setShowSuccess(false)
    setIsActive(false)
    const SERVER_URL = "https://hedbotecommerce.herokuapp.com"
    pageInfo !== "" &&
      axios
        .get(`${SERVER_URL}/api/bot/${pageInfo.id}`)
        .then(response => {
          setIsActive(response.data)
        })
        .catch(error => {
          // handle error
          console.log("error: ", error)
        })
  }, [pageInfo, pageInfo.id])

  const [showBusinessId, setShowBusinessId] = useState(true)
  console.log("selected:", pageInfo)
  if (pageInfo === "")
    return (
      <h1 style={{ padding: 50, width: "100%" }}>{"<-"} Click on one page</h1>
    )

  if (showSuccess) return <SuccessConnected pageId={pageInfo.id} />

  return (
    <div style={{ padding: 50, width: "100%" }}>
      <h1>{pageInfo.name}</h1>
      {isActive && isActive.active ? (
        <Paper
          style={{ backgroundColor: "green", color: "white", padding: 10 }}
        >
          <Typography variant="h5" component="h3">
            Connected{" "}
            <Button
              variant="contained"
              onClick={() => _pauseResume(pageInfo.id, false)}
              style={{ backgroundColor: "white" }}
            >
              Unplug
            </Button>
          </Typography>
          <Typography component="p">
            BusinessId: {isActive.businessId} / CatalogId: {isActive.catalogId}
          </Typography>
        </Paper>
      ) : (
        <div>not connected</div>
      )}
      {!showBusinessId && (
        <div>
          <b>Business Id:</b> {state.businessId}{" "}
          <Button
            color="primary"
            onClick={() => {
              dispatch({ type: "businessId", payload: "" })
              setShowBusinessId(true)
            }}
          >
            // change ID
          </Button>
        </div>
      )}
      {showBusinessId && (
        <div style={{ height: 55, marginTop: 10 }}>
          <TextField
            id="outlined-name"
            label="Business Id"
            value={state.businessId}
            onChange={e =>
              dispatch({ type: "businessId", payload: e.target.value })
            }
            variant="outlined"
          />
          <Button
            style={{ height: "100%" }}
            disabled={state.businessId.length < 8}
            onClick={() => {
              _catalogsList(state.businessId)
              setShowBusinessId(false)
            }}
            variant="contained"
          >
            Submit
          </Button>
        </div>
      )}
      {state.catalogListLoading ? (
        <div>
          <img src={window.location.origin + "/spinner.svg"} />
        </div>
      ) : (
        !showBusinessId &&
        state.catalogsList && (
          <div>
            <h3>Select a catalog: </h3>
            <CatalogsList
              pageInfo={pageInfo}
              _connectPage={_connectPage}
              state={state}
              dispatch={dispatch}
              setShowSuccess={setShowSuccess}
            />
          </div>
        )
      )}
    </div>
  )
}

export default SelectedPage
