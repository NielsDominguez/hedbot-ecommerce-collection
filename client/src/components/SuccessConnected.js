import React from "react"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"

const SuccessConnected = ({ pageId }) => {
  return (
    <div
      style={{
        width: "100%",
        marginTop: 200,
        marginLeft: 70,
        textAlign: "center"
      }}
    >
      <Paper
        style={{
          textAlign: "center",
          backgroundColor: "green",
          color: "white",
          padding: 50
        }}
      >
        <Typography variant="h3" component="h3">
          connected successfully
        </Typography>
        <Typography component="p">
          test bot here:{" "}
          <a style={{ color: "white" }} href={`http://m.me/${pageId}`}>
            http://m.me/{pageId}
          </a>
        </Typography>
      </Paper>
    </div>
  )
}

export default SuccessConnected
