import React, { useEffect, useReducer, useState } from "react"
import FacebookLogin from "react-facebook-login"
import TextField from "@material-ui/core/TextField"
import Grid from "@material-ui/core/Grid"
import axios from "axios"
import PagesList from "../components/PagesList"
import SelectedPage from "../components/SelectedPage"

const initialState = {
  catalogsList: [
    {
      id: "",
      name: "",
      product_count: "",
      products: { data: [] }
    }
  ],
  businessId: "",
  catalogId: "",
  pagesList: [
    { name: "", access_token: "", picture: { data: { url: "" } }, id: "" }
  ]
}

const reducer = (state, action) => {
  switch (action.type) {
    case "catalogsList":
      return {
        ...state,
        catalogsList: action.payload.owned_product_catalogs.data
      }
    case "businessId":
      return { ...state, businessId: action.payload }
    case "catalogListLoading":
      return { ...state, catalogListLoading: action.payload }

    case "pagesList":
      return { ...state, pagesList: action.payload }

    default:
      throw new Error()
  }
}

const HomeContainers = () => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const [token, setToken] = useState("")
  const [pageInfo, setPageInfo] = useState("")
  const [isAuth, setIsAuth] = useState(false)
  const SERVER_URL = "https://hedbotecommerce.herokuapp.com"

  useEffect(() => {
    const getToken = localStorage.getItem("token") || token !== ""
    axios
      .get(`https://graph.facebook.com/me?access_token=${getToken}`)
      .then(response => {
        _getPagesList(getToken)
        return setIsAuth(true)
      })
      .catch(error => {
        // handle error
        console.log(error, "error")
      })
  }, [token])

  const _getPagesList = token => {
    axios
      .get(
        `https://graph.facebook.com/v3.3/me/accounts?fields=name,is_webhooks_subscribed,access_token,picture&access_token=${token}`
      )
      .then(response => {
        dispatch({ type: "pagesList", payload: response.data.data })
        console.log("my pages", response.data.data)
      })
      .catch(error => {
        console.log(error)
      })
  }

  const _catalogsList = idBusiness => {
    dispatch({ type: "catalogListLoading", payload: true })
    axios
      .post(`${SERVER_URL}/api/catalogs`, {
        idBusiness
      })
      .then(response => {
        console.log(response.data)
        dispatch({ type: "catalogsList", payload: response.data })
        return dispatch({ type: "catalogListLoading", payload: false })
      })
      .catch(error => {
        console.log(error, "_catalogsList")
        return dispatch({ type: "catalogListLoading", payload: false })
      })
  }

  const _connectPage = (idPage, tokenPage, namePage, businessId, catalogId) => {
    axios
      .post(`${SERVER_URL}/api/add-bot`, {
        idPage,
        tokenPage,
        namePage,
        businessId,
        catalogId
      })
      .then(() => {
        return console.log("page connect success + db")
      })
      .catch(error => {
        return console.log("error connect", error)
      })
  }

  const _pauseResume = (idPage, active) => {
    axios
      .post(`${SERVER_URL}/api/bot-pause-or-resume`, { idPage, active })
      .then(() => {
        return window.location.reload()
      })
      .catch(error => {
        return console.log("error delete bot", error)
      })
  }

  return (
    <div>
      <a href={`${SERVER_URL}/api/login-fb`}>connect fb</a>
      {isAuth && (
        <div>
          <div
            style={{
              padding: 20
            }}
          >
            <Grid container direction="row" spacing={2}>
              <Grid
                style={{
                  overflowY: "scroll",
                  height: window.innerHeight - 20,
                  borderRight: "1px solid #dbdbdb"
                }}
                md={3}
                xs={12}
                spacing={2}
              >
                <PagesList
                  setPageInfo={setPageInfo}
                  pagesList={state.pagesList}
                  _connectPage={_connectPage}
                />
              </Grid>
              <Grid md={8} xs={12} spacing={2}>
                <SelectedPage
                  state={state}
                  _catalogsList={_catalogsList}
                  pageInfo={pageInfo}
                  state={state}
                  _connectPage={_connectPage}
                  _pauseResume={_pauseResume}
                  dispatch={dispatch}
                />
              </Grid>
            </Grid>
          </div>
        </div>
      )}
    </div>
  )
}

export default HomeContainers
