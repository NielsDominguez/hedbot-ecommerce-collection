import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter } from "react-router-dom"
import "./index.css"
import { StateInspector } from "reinspect"
import Router from "./Router"
import * as serviceWorker from "./serviceWorker"

ReactDOM.render(
  <StateInspector name="App">
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  </StateInspector>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
