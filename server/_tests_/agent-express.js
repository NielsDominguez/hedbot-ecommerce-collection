const request = require("supertest")
const app = require("../index")

const agent = request.agent(app)

module.exports = agent
