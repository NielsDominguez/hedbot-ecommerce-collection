const ModelBot = require("../../models/ModelBot")
const bson = require("bson")

describe("ModelBot models", () => {
  it("find after create", async () => {
    let mockOID = new bson.ObjectID()
    await ModelBot.create({
      _id: mockOID,
      namePage: "mock_namePage",
      idPage: "mock_idPage",
      tokenPage: "mock_tokenPage",
      businessId: "mock_businessId",
      catalogId: "mock_catalogId",
      active: true
    })

    let someInsertedSchema = await ModelBot.findOne()
      .where("_id")
      .equals(mockOID)

    expect(someInsertedSchema._id).toEqual(mockOID)
    expect(someInsertedSchema.namePage).toEqual("mock_namePage")
    expect(someInsertedSchema.idPage).toEqual("mock_idPage")
    expect(someInsertedSchema.tokenPage).toEqual("mock_tokenPage")
    expect(someInsertedSchema.businessId).toEqual("mock_businessId")
    expect(someInsertedSchema.catalogId).toEqual("mock_catalogId")
    expect(someInsertedSchema.active).toEqual(true)
  })
})
