const app = require("../../index.js")
const request = require("supertest")

describe("GET /webhook", () => {
  it("responds with json", done => {
    request(app)
      .get("/api/test")
      .set("Accept", "application/json")
      .expect("Content-Type", "text/html; charset=utf-8")
      .expect(200, done)
  })
})
