const mongoose = require("mongoose")
const mms = require("mongodb-memory-server")

// Load models since we will not be instantiating our express server.
require("../models/ModelAdmin")
require("../models/ModelBot")

beforeEach(async done => {
  /*
    Define clearDB function that will loop through all 
    the collections in our mongoose connection and drop them.
  */
  async function clearDB() {
    for (var i in mongoose.connection.collections) {
      await mongoose.connection.collections[i].drop(function() {})
    }
    return done()
  }

  /*
    If the mongoose connection is closed, 
    start it up using the test url and database name
    provided by the node runtime ENV
  */

  const mongoServer = new mms.MongoMemoryServer()

  await mongoServer.getConnectionString().then(async mongoFakeUrl => {
    if (mongoose.connection.readyState === 0) {
      await mongoose.connect(
        mongoFakeUrl, // <------- IMPORTANT
        { useNewUrlParser: true },
        function(err) {
          if (err) {
            throw err
          }
          return clearDB()
        }
      )
    } else {
      return clearDB()
    }
  })
})

afterEach(async done => {
  await mongoose.disconnect()
  return done()
})

afterAll(done => {
  return done()
})

test("mock test suite", () => {
  expect(process.env.NODE_ENV).toEqual("test")
})
