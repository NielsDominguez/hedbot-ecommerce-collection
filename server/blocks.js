const axios = require("axios")
require("dotenv").config()

const userToken =
  "EAAFiAgWw5XIBAFRzhtzBxVIdMPXQ7qoBcxmdyR5VB32B2kwNfh8G3wWLs9o2Ry8LjwfKaexoOUlZCyNRtOkJytifugBueSCV6577NNTV9qJNSoDDvjqSWB0WZA4UjlZCGsnd3i8SknTkRFFvbc55TfMaP5w9ndX8g6SFm43NAZDZD"

module.exports = {
  blockTxt: (text, PSID, tokenPage, catalogId) => {
    return axios
      .post(
        `https://graph.facebook.com/v2.6/me/messages?access_token=${tokenPage}`,
        {
          recipient: {
            id: PSID
          },
          messaging_type: "response",
          message: {
            text
          }
        }
      )
      .then(function(response) {
        console.log("send txt success ")
      })
      .catch(function(error) {
        console.log("error txt")
      })
  },

  quickRep: (PSID, tokenPage, catalogId) => {
    const API_CATEGORIES = `https://graph.facebook.com/v3.3/${catalogId}?fields=products{category,name,availability,brand,price}&access_token=${userToken}`

    axios
      .get(API_CATEGORIES, {
        headers: {
          access_token: userToken
        }
      })
      .then(response => {
        const listCat = response.data.products.data.map(x => x.category)
        const categories = [...new Set(listCat)]
        const btnList = categories.map(category => {
          const lastCatWord = category.split(" ").splice(-1)[0]
          return {
            content_type: "text",
            title: `category: ${lastCatWord}`,
            payload: `category: ${lastCatWord}`
          }
        })

        const quickRepBody = {
          text: "Welcome to our shop, select a category:",
          quick_replies: btnList
        }
        return axios
          .post(
            `https://graph.facebook.com/v2.6/me/messages?access_token=${tokenPage}`,
            {
              recipient: {
                id: PSID
              },
              messaging_type: "response",
              message: quickRepBody
            }
          )
          .then(function(response) {
            console.log("send txt success")
          })
          .catch(function(error) {
            console.log("error quick rep", error)
          })
      })
  },

  carousel: (PSID, category, tokenPage, catalogId) => {
    const API_PRODUCT = `https://graph.facebook.com/v3.3/${catalogId}?fields=products{category,description,price,name,availability,image_url}&access_token=${userToken}`

    axios
      .get(API_PRODUCT, {
        headers: {
          access_token: userToken
        }
      })
      .then(response => {
        console.log("category selected", category)
        console.log("hey", response.data.products.data)
        const productsFromCategory = response.data.products.data.filter(x =>
          x.category.includes(category)
        )
        console.log("------------------------", productsFromCategory)

        const slides = productsFromCategory.map(
          ({ category, price, description, name, availability, image_url }) => {
            return {
              title: name,
              subtitle: `(${availability}) ${description}`,
              image_url,
              buttons: [
                {
                  type: "postback",
                  title: `buy ${name} ${price}`,
                  payload: `buy ${name} ${price}`
                }
              ]
            }
          }
        )

        const carouselBody = {
          attachment: {
            type: "template",
            payload: {
              template_type: "generic",
              elements: slides
            }
          }
        }
        return axios
          .post(
            `https://graph.facebook.com/v2.6/me/messages?access_token=${tokenPage}`,
            {
              recipient: {
                id: PSID
              },
              messaging_type: "response",
              message: carouselBody
            }
          )
          .then(function(response) {
            console.log("send txt success")
          })
          .catch(function(error) {
            console.log("error carousel")
          })
      })
  }
}
