const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const path = require("path")
const mongoose = require("mongoose")
require("dotenv").config()
const axios = require("axios")
const block = require("./blocks")

const { PORT, DB_URI, DB_URI_PROD, NODE_ENV } = process.env

mongoose.connect(
  NODE_ENV === "production" ? DB_URI_PROD : DB_URI_PROD,
  { useNewUrlParser: true, useCreateIndex: true },
  () => console.log("mongo connected")
)

const app = express()
app.use(cors())

app.use(bodyParser.json())

NODE_ENV === "production" &&
  app.use(express.static(path.join(__dirname, "client/build")))

app.post("/webhook", (req, res) => {
  const SERVER_URL = "https://hedbotecommerce.herokuapp.com"
  const { body } = req
  if (body.object === "page") {
    body.entry.forEach(entry => {
      entry.messaging.forEach(event => {
        const PSID = event.sender.id
        const PAGE_ID = event.recipient.id
        if (event.message && event.message.text) {
          console.log("PSID:", PSID)
          console.log("PAGE_ID:", PAGE_ID)
          console.log(entry.messaging[0].message.text)
          axios
            .get(`${SERVER_URL}/api/findBot/${PAGE_ID}`)
            .then(response => {
              const { tokenPage, catalogId, active } = response.data
              if (response.data.active) {
                if (entry.messaging[0].message.text === "yo") {
                  return block.blockTxt("yo", PSID, tokenPage, catalogId)
                }
                if (entry.messaging[0].message.text === "test") {
                  return block.blockTxt(
                    "select our products",
                    PSID,
                    tokenPage,
                    catalogId
                  )
                }
                if (entry.messaging[0].message.text === "info") {
                  return block.blockTxt(
                    `${
                      catalogId &&
                      response.data.businessId &&
                      response.data.namePage
                        ? "Status OK"
                        : "Status not OK"
                    } Catalog ID: ${catalogId}, business ID: ${
                      response.data.businessId
                    }, page: ${response.data.namePage}`,
                    PSID,
                    tokenPage,
                    catalogId
                  )
                }

                if (entry.messaging[0].message.text === "start") {
                  block.quickRep(PSID, tokenPage, catalogId)
                }

                if (entry.messaging[0].message.text.includes("category")) {
                  block.carousel(
                    PSID,
                    entry.messaging[0].message.text.split(" ").splice(-1)[0],
                    tokenPage,
                    catalogId
                  )
                }
              }
            })
            .catch(function(error) {
              // handle error
              console.log(error)
            })
        }
      })
    })
    res.status(200).send("EVENT_RECEIVED")
  } else {
    res.sendStatus(404)
  }
})

app.get("/webhook", (req, res) => {
  let VERIFY_TOKEN = "<YOUR_VERIFY_TOKEN>"

  let mode = req.query["hub.mode"]
  let token = req.query["hub.verify_token"]
  let challenge = req.query["hub.challenge"]

  if (mode && token) {
    if (mode === "subscribe" && token === VERIFY_TOKEN) {
      console.log("WEBHOOK_VERIFIED")
      res.status(200).send(challenge)
    } else {
      res.sendStatus(403)
    }
  }
})

app.use("/api", require("./routes"))

NODE_ENV === "production" &&
  app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname + "/client/build/index.html"))
  })

if (process.env.NODE_ENV !== "test") {
  app.listen(PORT || 5000, () =>
    console.log("node start enviroment: " + process.env.NODE_ENV)
  )
}

module.exports = app
