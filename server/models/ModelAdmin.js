const mongoose = require("mongoose")

const AdminSchema = mongoose.Schema(
  {
    fbId: { type: String, lowercase: true, required: true },
    name: { type: String, lowercase: true },
    botId: [{ type: mongoose.Schema.ObjectId, ref: "Bots" }]
  },
  { timestamps: true }
)

const ModelAdmin = mongoose.model("Admins", AdminSchema)

module.exports = ModelAdmin
