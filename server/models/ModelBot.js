const mongoose = require("mongoose")

const BotSchema = mongoose.Schema(
  {
    namePage: { type: String, unique: true, required: true },
    adminId: { type: mongoose.Schema.ObjectId, ref: "Admins" },
    idPage: { type: String, unique: true, required: true },
    tokenPage: { type: String, required: true },
    businessId: { type: String, required: true },
    catalogId: { type: String, required: true },
    active: Boolean
  },
  { timestamps: true }
)

const ModelBot = mongoose.model("Bots", BotSchema)

module.exports = ModelBot
