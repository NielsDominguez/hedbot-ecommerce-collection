const router = require("express").Router()
const axios = require("axios")
const mongoose = require("mongoose")
require("dotenv").config()
const ModelBot = require("./models/ModelBot")
const ModelAdmin = require("./models/ModelAdmin")

require("dotenv").config()

const { USER_TOKEN } = process.env

router.get("/test", (req, res, next) => {
  res.send("test")
})

router.post("/add-bot", (req, res, next) => {
  const {
    tokenPage,
    idPage,
    namePage,
    businessId,
    catalogId,
    adminId
  } = req.body
  axios
    .post(
      `https://graph.facebook.com/v3.3/${idPage}/subscribed_apps?subscribed_fields=mention,feed,name,picture,category,conversations,description,conversations,branded_camera,feature_access_list,standby,messages,messaging_account_linking,messaging_checkout_updates,message_echoes,message_deliveries,messaging_game_plays,messaging_optins,messaging_optouts,messaging_payments,messaging_postbacks,messaging_pre_checkouts,message_reads,messaging_referrals,messaging_handovers,messaging_policy_enforcement,messaging_page_feedback,messaging_appointments,messaging_direct_sends,founded,company_overview,mission,products,general_info,location,hours,parking,public_transit,commerce_order,phone,email,page_about_story,website,payment_options,page_upcoming_change,page_change_proposal&access_token=${tokenPage}`
    )
    .then(() => {
      console.log("1")
      ModelBot.findOneAndUpdate(
        { idPage },
        {
          adminId,
          tokenPage,
          idPage,
          namePage,
          businessId,
          catalogId,
          active: true
        },
        { upsert: true, new: true, setDefaultsOnInsert: true },
        (err, docBot) => {
          if (err)
            return res.status(400).json({
              error_msg: "error query find one and update"
            })
          ModelAdmin.findOneAndUpdate(
            { _id: adminId, botId: { $ne: docBot._id } },
            { $addToSet: { botId: docBot._id } },
            function(err, docAdmin) {
              res.json({ docBot })
            }
          )
        }
      )
    })
    .catch(e => res.send(e))
})

router.post("/bot-pause-or-resume", (req, res, next) => {
  const { idPage, active } = req.body
  ModelBot.findOneAndUpdate(
    { idPage },
    {
      active
    },
    (err, doc) => {
      if (err)
        return res.status(400).json({
          error_msg: "error query"
        })
      res.send("success")
    }
  )
})

//graph.facebook.com/v3.2/202390663640624?fields=owned_product_catalogs{name,product_count,products{category,name,availability,image_url}}&access_token=${USER_TOKEN}

router.post("/catalogs", (req, res, next) => {
  const { idBusiness } = req.body
  return axios
    .post(
      `https://graph.facebook.com/v3.2/${idBusiness}?fields=owned_product_catalogs{name,product_count,products{category,name,availability,image_url}}&access_token=${USER_TOKEN}`
    )
    .then(response => {
      console.log(response.data)
      res.json(response.data)
    })
    .catch(function(error) {
      console.log("error txt")
    })
})

router.get("/bot/:idPage", (req, res, next) => {
  const { idPage } = req.params
  ModelBot.findOne({ idPage }, (err, bot) => {
    return err
      ? res.status(400).json({
          error_msg: err
        })
      : !bot
      ? res.status(400).json({
          error_msg: "cannot find bot"
        })
      : res.json(bot)
  })
})

router.get("/long-lived-token/:shortLivedToken", (req, res) => {
  const { shortLivedToken } = req.params
  const app_id = 389235801646450
  const client_secret = "1eb8d349b5f158980afa1f219fe3706f"
  axios
    .get(
      `https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${app_id}&client_secret=${client_secret}&fb_exchange_token=${shortLivedToken}`
    )
    .then(response => {
      res.json(response.data)
    })
    .catch(error => {
      console.log(error)
    })
})

router.get("/login-fb", (req, res) => {
  const app_id = "389235801646450"
  const uri = "https://609aaf3f.ngrok.io/api/callbackfb"
  const scope =
    "pages_show_list, manage_pages, ads_management, ads_read, business_management, pages_messaging, pages_messaging_phone_number, pages_messaging_subscriptions"

  res.redirect(
    `https://www.facebook.com/v3.2/dialog/oauth?client_id=${app_id}&redirect_uri=${uri}&scope=${scope}`
  )
})

router.get("/callbackfb", (req, res) => {
  const app_id = "389235801646450"
  const client_secret = "1eb8d349b5f158980afa1f219fe3706f"
  const { code } = req.query
  const server = "https://609aaf3f.ngrok.io"
  const uri = `${server}/api/callbackfb`
  //graph.facebook.com/v3.2/oauth/access_token?client_id=389235801646450&redirect_uri=https://hbotecommercebotclient.serveo.net&client_secret=1eb8d349b5f158980afa1f219fe3706f&code=AQB7ZYuxbRaS-PwU31tAmjqmxQZoryA5jn2yN6m5EqPS6WT1nKYWfpiI27_RYBowOddNIY-daFjB4nfwFZ_wTwgnZszq1i0kebP9tS2nVykwbomuxO_QThuliNZ9460dCv5pZJi8wj_D9IuCAInRUNl6GmYIhW09-ZeZ3qbdsiXHRgvtXJReQDKdpAa8Dy_CqnDxM1fgz6vooVHzZD_9iSzqjXx-VVao34rAnfAG5yBabreVn9emmy4l2INavuwiA6npkIQQc4xlAeOsBKvafY8TzOOwkVpW-JAagN_ffWucsTxocNZ8dBgpKbVU91-ufQ7i1eelc84Jsyq58VKi4ZvH
  axios
    .get(
      `https://graph.facebook.com/v3.2/oauth/access_token?client_id=${app_id}&redirect_uri=${uri}&client_secret=${client_secret}&code=${code}`
    )
    .then(response =>
      axios
        .get(`${server}/api/long-lived-token/${response.data.access_token}`)
        .then(response => {
          res.redirect(
            `https://hedbotecommerce.herokuapp.com/${
              response.data.access_token
            }`
          )
        })
        .catch(error => {
          console.log(error.message)
          res.send(
            `app id: ${app_id}, client_secret: ${client_secret}, code: ${code}, uri: ${uri} ${JSON.stringify(
              error.message
            )}`
          )
        })
    )
})

router.get("/admin", (req, res) => {
  const token = req.header("access_token")
  axios
    .get(
      `https://graph.facebook.com/v3.2/me?fields=id,name&access_token=${token}`
    )
    .then(graphResp => {
      ModelAdmin.findOne({ fbId: graphResp.data.id })
        .populate("botId")
        .exec(function(err, docs) {
          if (err)
            return res.status(400).json({
              error_msg: "error query"
            })
          if (docs === null) {
            const newAdmin = new ModelAdmin({
              fbId: graphResp.data.id,
              name: graphResp.data.name
            })

            newAdmin.save((errSave, savedAdmin) => {
              if (errSave) {
                res.status(400).json({
                  error_msg: errSave.error_msg
                })
              }
              console.log("is saved", savedAdmin)
              res.json(savedAdmin)
            })
          }
          if (docs !== null) return res.json(docs)
        })
    })
})

module.exports = router
